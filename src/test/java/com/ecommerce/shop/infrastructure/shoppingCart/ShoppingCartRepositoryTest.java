package com.ecommerce.shop.infrastructure.shoppingCart;


import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCart;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartRepository;
import com.ecommerce.shop.domain.model.product.Product;
import com.ecommerce.shop.infrastructure.shoppingcart.ShoppingCartRepositoryImpl;
import io.quarkus.test.junit.QuarkusTest;
import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


/**
 * The Class ShoppingCartRepositoryTest.
 */
@QuarkusTest
public class ShoppingCartRepositoryTest {

	ShoppingCartRepository shoppingCartRepository;

	@Before
	public void setUp() {
		shoppingCartRepository = new ShoppingCartRepositoryImpl();
	}

	@Test
	public void shouldCreateDataRequested()
	{
		EasyRandom generator = new EasyRandom();
		ShoppingCart shoppingCart = generator.nextObject(ShoppingCart.class);
		shoppingCartRepository.create(shoppingCart);
	}

	@Test
	public void givenShoppingCartInRepository_whenFindByUserId_thenShouldReturnShoppingCartFromRepository() throws ShoppingCartNotFoundException {
		EasyRandom generator = new EasyRandom();
		ShoppingCart shoppingCart = generator.nextObject(ShoppingCart.class);
		shoppingCartRepository.create(shoppingCart);
		Assert.assertEquals(shoppingCart, shoppingCartRepository.load(shoppingCart.userId()));
	}

	@Test
	public void updateShoppingCartInRepository_thenShouldReturnShoppingCartUpdatedFromRepository() throws ShoppingCartNotFoundException {
		EasyRandom generator = new EasyRandom();
		Product product = generator.nextObject(Product.class);
		ShoppingCart shoppingCart = generator.nextObject(ShoppingCart.class);
		shoppingCart.addToShoppingCart(product);
		shoppingCartRepository.create(shoppingCart);
		product.updateProduct(new Product(product.productId(), "FakeProduct", 3));
		shoppingCart.updateShoppingCart(product);
		shoppingCartRepository.update(shoppingCart);
		Assert.assertEquals(shoppingCart, shoppingCartRepository.load(shoppingCart.userId()));

	}

	@Test (expected = ShoppingCartNotFoundException.class)
	public void deleteShoppingCartInRepository_thenLoadShouldThrowShoppingCartNotFoundException() throws ShoppingCartNotFoundException {
		EasyRandom generator = new EasyRandom();
		ShoppingCart shoppingCart = generator.nextObject(ShoppingCart.class);;
		shoppingCartRepository.create(shoppingCart);
		shoppingCartRepository.delete(shoppingCart);
		shoppingCartRepository.load(shoppingCart.userId());
	}
}