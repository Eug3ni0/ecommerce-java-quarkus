package com.ecommerce.shop.infrastructure.user;


import com.ecommerce.shop.domain.model.user.UserId;
import com.ecommerce.shop.domain.model.user.UserRepository;
import io.quarkus.test.junit.QuarkusTest;
import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


/**
 * The Class UserRepositoryTest.
 */
@QuarkusTest
public class UserRepositoryTest {

	UserRepository userRepository;

	@Before
	public void setUp() {
		userRepository = new UserRepositoryImpl();
	}

	@Test
	public void shouldInsertDataRequested()
	{
		EasyRandom generator = new EasyRandom();
		UserId userId = generator.nextObject(UserId.class);
		UserId userIdCreated = userRepository.create(userId);
		Assert.assertEquals(userId, userIdCreated);
	}
}
