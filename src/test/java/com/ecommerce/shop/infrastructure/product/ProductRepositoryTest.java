package com.ecommerce.shop.infrastructure.product;


import com.ecommerce.shop.domain.model.product.Product;
import com.ecommerce.shop.domain.model.product.ProductNotFoundException;
import com.ecommerce.shop.domain.model.product.ProductRepository;
import io.quarkus.test.junit.QuarkusTest;
import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


/**
 * The Class ProductRepositoryTest.
 */
@QuarkusTest
public class ProductRepositoryTest {

	ProductRepository productRepository;

	@Before
	public void setUp() {
		productRepository = new ProductRepositoryImpl();
	}

	@Test
	public void shouldInsertDataRequested()
	{
		EasyRandom generator = new EasyRandom();
		Product product = generator.nextObject(Product.class);
		productRepository.insert(product);
	}


	@Test
	public void givenProductInRepository_whenFindByProductId_thenShouldReturnProductFromRepository() throws ProductNotFoundException {
		EasyRandom generator = new EasyRandom();
		Product product = generator.nextObject(Product.class);
		productRepository.insert(product);
		Assert.assertEquals(product, productRepository.load(product.productId()));
	}

	@Test
	public void updateProductInRepository_thenShouldReturnProductUpdatedFromRepository() throws ProductNotFoundException {
		EasyRandom generator = new EasyRandom();
		Product product = generator.nextObject(Product.class);
		productRepository.insert(product);
		product.updateProduct(generator.nextObject(Product.class));
		productRepository.update(product);
		Assert.assertEquals(product, productRepository.load(product.productId()));
	}


}
