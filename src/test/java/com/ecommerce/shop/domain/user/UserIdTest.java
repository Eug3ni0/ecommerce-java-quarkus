package com.ecommerce.shop.domain.user;


import com.ecommerce.shop.domain.model.user.UserId;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.Assert;
import org.junit.Test;


/**
 * The Class UserIdTest.
 */
@QuarkusTest
public class UserIdTest {

	@Test
	public void shouldIdIncrementOne() {
		UserId firstUserId = new UserId();
		UserId secondUserId = new UserId();
		Assert.assertEquals(firstUserId.id() + 1, secondUserId.id());
	}
}