package com.ecommerce.shop.domain.product;


import com.ecommerce.shop.domain.model.product.ProductId;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.Assert;
import org.junit.Test;


/**
 * The Class ProductIdTest.
 */
@QuarkusTest
public class ProductIdTest {

	@Test
	public void shouldIdIncrementOne() {
		ProductId firstProductId = new ProductId();
		ProductId secondtProductId = new ProductId();
		Assert.assertEquals(firstProductId.id() + 1, secondtProductId.id());
	}
}