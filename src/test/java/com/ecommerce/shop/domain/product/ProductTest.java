package com.ecommerce.shop.domain.product;


import com.ecommerce.shop.domain.model.product.Product;
import io.quarkus.test.junit.QuarkusTest;
import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Test;


/**
 * The Class ProductTest.
 */
@QuarkusTest
public class ProductTest {

	@Test
	public void shouldUpdateProduct() {

		EasyRandom generator = new EasyRandom();
		Product product = generator.nextObject(Product.class);
		Product updated = new Product(product.productId(), "Product updated", 3);
		product.updateProduct(updated);
		Assert.assertEquals("Product updated", product.description());
		Assert.assertEquals(3, product.amount());
	}
}