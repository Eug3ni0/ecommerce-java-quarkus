package com.ecommerce.shop.domain.shoppingcart;


import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartId;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.Assert;
import org.junit.Test;


/**
 * The Class ShoppingCartId.
 */
@QuarkusTest
public class ShoppingCartIdTest {

	@Test
	public void shouldIdIncrementOne() {
		ShoppingCartId firstShoppingCartId = new ShoppingCartId();
		ShoppingCartId secondShoppingCartId = new ShoppingCartId();
		Assert.assertEquals(firstShoppingCartId.id() + 1, secondShoppingCartId.id());
	}
}