package com.ecommerce.shop.domain.shoppingcart;


import com.ecommerce.shop.domain.model.product.Product;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCart;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartId;
import com.ecommerce.shop.domain.model.user.UserId;
import io.quarkus.test.junit.QuarkusTest;
import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Test;


/**
 * The Class ShoppingCartTest.
 */
@QuarkusTest
public class ShoppingCartTest {

	@Test
	public void shouldInsertInShoppingCart() {

		EasyRandom generator = new EasyRandom();
		Product product = generator.nextObject(Product.class);
		ShoppingCart shoppingCart = new ShoppingCart(new ShoppingCartId(), new UserId());
		shoppingCart.addToShoppingCart(product);

		Assert.assertEquals(product, shoppingCart.product().get(0));
	}

	@Test
	public void shouldUpdateInShoppingCart(){
		EasyRandom generator = new EasyRandom();
		Product product = generator.nextObject(Product.class);
		ShoppingCart shoppingCart = new ShoppingCart(new ShoppingCartId(), new UserId());
		shoppingCart.addToShoppingCart(product);
		Product updated = new Product(product.productId(), "Product updated", 3);
		product.updateProduct(updated);
		shoppingCart.updateShoppingCart(product);

		Assert.assertEquals("Product updated", shoppingCart.product().get(0).description());
		Assert.assertEquals(3, shoppingCart.product().get(0).amount());
	}
}