package com.ecommerce.shop.application.shoppingCart;


import com.ecommerce.shop.application.service.shoppingCart.GetShoppingCartCommand;
import com.ecommerce.shop.application.service.shoppingCart.GetShoppingCartService;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCart;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartRepository;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;


/**
 * The Class GetShoppingCartServiceTest.
 */
@QuarkusTest
public class GetShoppingCartServiceTest {

	@Test
	public void shouldGetShoppingCartRepository() throws ShoppingCartNotFoundException {
		ShoppingCart shoppingCart = Mockito.mock(ShoppingCart.class);
		GetShoppingCartCommand getShoppingCartCommand = Mockito.mock(GetShoppingCartCommand.class);
		ShoppingCartRepository mock = Mockito.mock(ShoppingCartRepository.class);
		Mockito.when(mock.load(Mockito.any())).thenReturn(shoppingCart);
		GetShoppingCartService getShoppingCartService = new GetShoppingCartService(mock);
		ShoppingCart response = getShoppingCartService.execute(getShoppingCartCommand);
		Assert.assertEquals(shoppingCart, response);
	}

	@Test (expected = ShoppingCartNotFoundException.class)
	public void shouldThrowShoppingCartNotFoundExceptionFromRepository() throws ShoppingCartNotFoundException {
		GetShoppingCartCommand getShoppingCartCommand = Mockito.mock(GetShoppingCartCommand.class);
		ShoppingCartRepository mock = Mockito.mock(ShoppingCartRepository.class);
		Mockito.when(mock.load(Mockito.any())).thenThrow(ShoppingCartNotFoundException.class);
		GetShoppingCartService getShoppingCartService = new GetShoppingCartService(mock);
		getShoppingCartService.execute(getShoppingCartCommand);
	}
}