package com.ecommerce.shop.application.shoppingCart;


import com.ecommerce.shop.application.service.shoppingCart.PutShoppingCartCommand;
import com.ecommerce.shop.application.service.shoppingCart.PutShoppingCartService;
import com.ecommerce.shop.domain.model.product.Product;
import com.ecommerce.shop.domain.model.product.ProductNotFoundException;
import com.ecommerce.shop.domain.model.product.ProductRepository;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCart;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartRepository;
import com.ecommerce.shop.domain.model.user.UserId;
import com.ecommerce.shop.domain.model.user.UserNotFoundException;
import com.ecommerce.shop.domain.model.user.UserRepository;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;


/**
 * The Class PutShoppingCartServiceTest.
 */
@QuarkusTest
public class PutShoppingCartServiceTest {

	ShoppingCartRepository shoppingCartRepository;

	ProductRepository productRepository;

	UserRepository userRepository;

	PutShoppingCartCommand putShoppingCartCommand;

	@Before
	public void setUp() {
		shoppingCartRepository = Mockito.mock(ShoppingCartRepository.class);
		productRepository = Mockito.mock(ProductRepository.class);
		userRepository = Mockito.mock(UserRepository.class);
		putShoppingCartCommand = Mockito.mock(PutShoppingCartCommand.class);
	}

	@Test (expected = ProductNotFoundException.class)
	public void shouldThrowProductNotFoundFromRepository() throws ProductNotFoundException, ShoppingCartNotFoundException, UserNotFoundException {
		Mockito.when(productRepository.load(Mockito.any())).thenThrow(ProductNotFoundException.class);
		PutShoppingCartService putShoppingCartService = new PutShoppingCartService(shoppingCartRepository, productRepository, userRepository);
		putShoppingCartService.execute(putShoppingCartCommand);
	}

	@Test (expected = UserNotFoundException.class)
	public void shouldThrowUserNotFoundFromRepository() throws UserNotFoundException, ShoppingCartNotFoundException, ProductNotFoundException {
		Product product = Mockito.mock(Product.class);
		Mockito.when(productRepository.load(Mockito.any())).thenReturn(product);
		Mockito.when(userRepository.load(Mockito.any())).thenThrow(UserNotFoundException.class);
		PutShoppingCartService putShoppingCartService = new PutShoppingCartService(shoppingCartRepository, productRepository, userRepository);
		putShoppingCartService.execute(putShoppingCartCommand);
	}

	@Test (expected = ShoppingCartNotFoundException.class)
	public void shouldThrowShoppingCartNotFoundFromRepository() throws UserNotFoundException, ShoppingCartNotFoundException, ProductNotFoundException {
		Product product = Mockito.mock(Product.class);
		UserId userId = Mockito.mock(UserId.class);
		Mockito.when(productRepository.load(Mockito.any())).thenReturn(product);
		Mockito.when(userRepository.load(Mockito.any())).thenReturn(userId);
		Mockito.when(shoppingCartRepository.load(userId)).thenThrow(ShoppingCartNotFoundException.class);
		PutShoppingCartService putShoppingCartService = new PutShoppingCartService(shoppingCartRepository, productRepository, userRepository);
		putShoppingCartService.execute(putShoppingCartCommand);
	}

	@Test
	public void shouldShoppingCartRepositoryHaveBeenCalled() throws UserNotFoundException, ShoppingCartNotFoundException, ProductNotFoundException {
		Product product = Mockito.mock(Product.class);
		UserId userId = Mockito.mock(UserId.class);
		ShoppingCart shoppingCart = Mockito.mock(ShoppingCart.class);
		Mockito.when(productRepository.load(Mockito.any())).thenReturn(product);
		Mockito.when(userRepository.load(Mockito.any())).thenReturn(userId);
		Mockito.when(shoppingCartRepository.load(userId)).thenReturn(shoppingCart);
		PutShoppingCartService putShoppingCartService = new PutShoppingCartService(shoppingCartRepository, productRepository, userRepository);
		putShoppingCartService.execute(putShoppingCartCommand);
		Mockito.verify(shoppingCartRepository, Mockito.times(1)).update(shoppingCart);
	}
}