package com.ecommerce.shop.application.shoppingCart;


import com.ecommerce.shop.application.service.shoppingCart.PostShoppingCartCommand;
import com.ecommerce.shop.application.service.shoppingCart.PostShoppingCartService;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartExistException;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartId;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartRepository;
import com.ecommerce.shop.domain.model.user.UserNotFoundException;
import com.ecommerce.shop.domain.model.user.UserRepository;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;


/**
 * The Class PostShoppingCartServiceTest.
 */
@QuarkusTest
public class PostShoppingCartServiceTest {

	@Test
	public void shouldInsertShoppingCartRepository() throws UserNotFoundException, ShoppingCartExistException, ShoppingCartNotFoundException {
		PostShoppingCartCommand command = Mockito.mock(PostShoppingCartCommand.class);
		ShoppingCartRepository shoppingCartRepository = Mockito.mock(ShoppingCartRepository.class);
		UserRepository userRepository = Mockito.mock(UserRepository.class);
		ShoppingCartId shoppingCartId = Mockito.mock(ShoppingCartId.class);
		Mockito.when(shoppingCartRepository.load(Mockito.any())).thenThrow(ShoppingCartNotFoundException.class);
		Mockito.when(shoppingCartRepository.create(Mockito.any())).thenReturn(shoppingCartId);
		PostShoppingCartService postShoppingCartService = new PostShoppingCartService(shoppingCartRepository, userRepository);
		ShoppingCartId expected = postShoppingCartService.execute(command);
		Assert.assertEquals(expected, shoppingCartId);
	}

	@Test (expected = ShoppingCartExistException.class)
	public void shouThrowShoppingCartExistExceptionWhenShoppingCartExistsInRepository() throws UserNotFoundException, ShoppingCartExistException {
		PostShoppingCartCommand command = Mockito.mock(PostShoppingCartCommand.class);
		ShoppingCartRepository shoppingCartRepository = Mockito.mock(ShoppingCartRepository.class);
		UserRepository userRepository = Mockito.mock(UserRepository.class);
		ShoppingCartId shoppingCartId = Mockito.mock(ShoppingCartId.class);
		Mockito.when(shoppingCartRepository.create(Mockito.any())).thenReturn(shoppingCartId);
		PostShoppingCartService postShoppingCartService = new PostShoppingCartService(shoppingCartRepository, userRepository);
		postShoppingCartService.execute(command);
	}

	@Test (expected = UserNotFoundException.class)
	public void shouThrowUserNotFoundExceptionWhenLoadUserInService() throws UserNotFoundException, ShoppingCartExistException {
		PostShoppingCartCommand command = Mockito.mock(PostShoppingCartCommand.class);
		ShoppingCartRepository shoppingCartRepository = Mockito.mock(ShoppingCartRepository.class);
		UserRepository userRepository = Mockito.mock(UserRepository.class);
		Mockito.when(userRepository.load(Mockito.any())).thenThrow(UserNotFoundException.class);
		PostShoppingCartService postShoppingCartService = new PostShoppingCartService(shoppingCartRepository, userRepository);
		postShoppingCartService.execute(command);
	}
}