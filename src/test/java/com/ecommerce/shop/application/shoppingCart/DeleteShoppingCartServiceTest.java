package com.ecommerce.shop.application.shoppingCart;


import com.ecommerce.shop.application.service.shoppingCart.DeleteShoppingCartCommand;
import com.ecommerce.shop.application.service.shoppingCart.DeleteShoppingCartService;
import com.ecommerce.shop.application.service.shoppingCart.GetShoppingCartCommand;
import com.ecommerce.shop.application.service.shoppingCart.GetShoppingCartService;
import com.ecommerce.shop.domain.model.product.Product;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCart;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartId;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartRepository;
import com.ecommerce.shop.domain.model.user.UserId;
import com.google.common.base.Verify;
import io.quarkus.test.junit.QuarkusTest;
import org.jeasy.random.EasyRandom;
import org.junit.Test;
import org.mockito.Mockito;


/**
 * The Class DeleteShoppingCartServiceTest.
 */
@QuarkusTest
public class DeleteShoppingCartServiceTest {

	@Test (expected = ShoppingCartNotFoundException.class)
	public void shouldThrowShoppingCartNotFoundExceptionFromRepository() throws ShoppingCartNotFoundException {
		DeleteShoppingCartCommand deleteShoppingCartCommand = Mockito.mock(DeleteShoppingCartCommand.class);
		ShoppingCartRepository mock = Mockito.mock(ShoppingCartRepository.class);
		Mockito.when(mock.load(Mockito.any())).thenThrow(ShoppingCartNotFoundException.class);
		DeleteShoppingCartService deleteShoppingCartService = new DeleteShoppingCartService(mock);
		deleteShoppingCartService.execute(deleteShoppingCartCommand);
	}

	@Test
	public void shouldDeleteShoppingCartRepositoryHaveBeenCalled() throws ShoppingCartNotFoundException {
		DeleteShoppingCartCommand deleteShoppingCartCommand = Mockito.mock(DeleteShoppingCartCommand.class);
		ShoppingCartRepository mock = Mockito.mock(ShoppingCartRepository.class);
		EasyRandom generator = new EasyRandom();
		ShoppingCart shoppingCart = generator.nextObject(ShoppingCart.class);
		Mockito.when(mock.load(Mockito.any())).thenReturn(shoppingCart);
		DeleteShoppingCartService deleteShoppingCartService = new DeleteShoppingCartService(mock);
		deleteShoppingCartService.execute(deleteShoppingCartCommand);
		Mockito.verify(mock, Mockito.times(1)).delete(shoppingCart);
	}
}