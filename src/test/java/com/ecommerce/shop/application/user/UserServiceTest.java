package com.ecommerce.shop.application.user;


import com.ecommerce.shop.application.service.user.UserCommand;
import com.ecommerce.shop.application.service.user.UserService;
import com.ecommerce.shop.domain.model.user.UserId;
import com.ecommerce.shop.domain.model.user.UserRepository;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;


/**
 * The Class UserServiceTest.
 */
@QuarkusTest
public class UserServiceTest {

	@Test
	public void shouldInsertUserRepository() {

		UserCommand userCommand = Mockito.mock(UserCommand.class);
		UserRepository userRepository = Mockito.mock(UserRepository.class);
		UserId userId = Mockito.mock(UserId.class);
		Mockito.when(userRepository.create(Mockito.any())).thenReturn(userId);
		UserService userService = new UserService(userRepository);
		UserId expected = userService.execute(userCommand);
		Assert.assertEquals(expected, userId);
	}


}
