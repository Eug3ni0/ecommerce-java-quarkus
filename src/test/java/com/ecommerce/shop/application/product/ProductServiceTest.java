package com.ecommerce.shop.application.product;


import com.ecommerce.shop.application.service.product.ProductCommand;
import com.ecommerce.shop.application.service.product.ProductService;
import com.ecommerce.shop.domain.model.product.Product;
import com.ecommerce.shop.domain.model.product.ProductId;
import com.ecommerce.shop.domain.model.product.ProductRepository;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;


/**
 * The Class ProductServiceTest.
 */
@QuarkusTest
public class ProductServiceTest {

	@Test
	public void shouldInsertProductRepository() {

		ProductCommand productCommand = Mockito.mock(ProductCommand.class);
		ProductRepository productRepository = Mockito.mock(ProductRepository.class);
		Product product = Mockito.mock(Product.class);
		Mockito.doReturn(product.productId()).when(productRepository).insert(product);
		ProductService productService = new ProductService(productRepository);
		ProductId expected = productService.execute(productCommand);
		Assert.assertEquals(expected, product.productId());
	}


}
