package com.ecommerce.shop.adapter.api.user;


import com.ecommerce.shop.application.service.user.UserService;
import com.ecommerce.shop.domain.model.user.UserId;
import com.google.gson.Gson;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import javax.ws.rs.core.Response;


/**
 * The Class PostUserControllerTest.
 */
@QuarkusTest
public class PostUserControllerTest {

	@Test
	public void shouldReturnResponseCreated() {

		UserService userService = Mockito.mock(UserService.class);
		UserId userId = new UserId();
		Mockito.when(userService.execute(Mockito.any())).thenReturn(userId);
		PostUserController postUserController = new PostUserController(userService);
		Response response = postUserController.createUser();
		Response expectedResponse =  Response.status(Response.Status.CREATED).entity(new Gson().toJson(userId)).build();

		Assert.assertEquals(expectedResponse.getStatus(), response.getStatus());
		Assert.assertEquals(expectedResponse.getEntity(), response.getEntity());
	}
}
