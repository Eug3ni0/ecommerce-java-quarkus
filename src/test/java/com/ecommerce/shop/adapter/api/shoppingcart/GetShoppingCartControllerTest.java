package com.ecommerce.shop.adapter.api.shoppingcart;


import com.ecommerce.shop.application.service.shoppingCart.GetShoppingCartService;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCart;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;
import com.google.gson.Gson;
import io.quarkus.test.junit.QuarkusTest;
import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.ws.rs.core.Response;
import java.util.Random;


/**
 * The Class GetShoppingCartControllerTest.
 */
@QuarkusTest
public class GetShoppingCartControllerTest {

	GetShoppingCartService getShoppingCartService;

	@Before
	public void setUp() {
		getShoppingCartService = Mockito.mock(GetShoppingCartService.class);
	}

	@Test
	public void shouldReturnOkResponseAndContainsShoppingCart() throws ShoppingCartNotFoundException {
		EasyRandom generator = new EasyRandom();
		ShoppingCart shoppingCart = generator.nextObject(ShoppingCart.class);
		Mockito.when(getShoppingCartService.execute(Mockito.any())).thenReturn(shoppingCart);
		GetShoppingCartController getShoppingCartController = new GetShoppingCartController(getShoppingCartService);
		Response response = getShoppingCartController.getShoppingCart(new Random().nextInt());
		Response expectedResponse =  Response.status(Response.Status.CREATED).entity(new Gson().toJson(shoppingCart)).build();

		Assert.assertEquals(expectedResponse.getStatus(), response.getStatus());
		Assert.assertEquals(expectedResponse.getEntity(), response.getEntity());
	}

	@Test (expected = ShoppingCartNotFoundException.class)
	public void shouldThrowShoppingCartNotFoundExceptionFromService() throws ShoppingCartNotFoundException {
		Mockito.when(getShoppingCartService.execute(Mockito.any())).thenThrow(ShoppingCartNotFoundException.class);
		GetShoppingCartController getShoppingCartController = new GetShoppingCartController(getShoppingCartService);
		getShoppingCartController.getShoppingCart(new Random().nextInt());
	}
}