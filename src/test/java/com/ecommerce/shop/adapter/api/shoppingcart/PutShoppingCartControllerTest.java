package com.ecommerce.shop.adapter.api.shoppingcart;


import com.ecommerce.shop.application.service.shoppingCart.PutShoppingCartService;
import com.ecommerce.shop.domain.model.product.ProductNotFoundException;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;
import com.ecommerce.shop.domain.model.user.UserNotFoundException;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.ws.rs.core.Response;


/**
 * The Class DeleteShoppingCartControllerTest.
 */
@QuarkusTest
public class PutShoppingCartControllerTest {

	PutShoppingCartService putShoppingCartService;
	PutShoppingCartRequest putShoppingCartRequest;


	@Before
	public void setUp() {
		putShoppingCartService = Mockito.mock(PutShoppingCartService.class);
		putShoppingCartRequest = Mockito.mock(PutShoppingCartRequest.class);
	}

	@Test
	public void shouldReturnOkFromPutRequest() throws UserNotFoundException, ShoppingCartNotFoundException, ProductNotFoundException {
		PutShoppingCartController putShoppingCartController = new PutShoppingCartController(putShoppingCartService);
		Response response = putShoppingCartController.insertProduct(putShoppingCartRequest);
		Response expectedResponse =  Response.status(Response.Status.OK).build();

		Assert.assertEquals(expectedResponse.getStatus(), response.getStatus());
	}

	@Test (expected = UserNotFoundException.class)
	public void shouldThrowUserNotFoundExceptionFromService() throws UserNotFoundException, ShoppingCartNotFoundException, ProductNotFoundException {
		Mockito.doThrow(UserNotFoundException.class).when(putShoppingCartService).execute(Mockito.any());
		PutShoppingCartController putShoppingCartController = new PutShoppingCartController(putShoppingCartService);
		putShoppingCartController.insertProduct(putShoppingCartRequest);
	}

	@Test (expected = ShoppingCartNotFoundException.class)
	public void shouldThrowShoppingCartNotFoundExceptionFromService() throws UserNotFoundException, ShoppingCartNotFoundException, ProductNotFoundException {
		Mockito.doThrow(ShoppingCartNotFoundException.class).when(putShoppingCartService).execute(Mockito.any());
		PutShoppingCartController putShoppingCartController = new PutShoppingCartController(putShoppingCartService);
		putShoppingCartController.insertProduct(putShoppingCartRequest);
	}

	@Test (expected = ProductNotFoundException.class)
	public void shouldThrowProductNotFoundExceptionFromService() throws UserNotFoundException, ShoppingCartNotFoundException, ProductNotFoundException {
		Mockito.doThrow(ProductNotFoundException.class).when(putShoppingCartService).execute(Mockito.any());
		PutShoppingCartController putShoppingCartController = new PutShoppingCartController(putShoppingCartService);
		putShoppingCartController.insertProduct(putShoppingCartRequest);
	}
}