package com.ecommerce.shop.adapter.api.shoppingcart;


import com.ecommerce.shop.application.service.shoppingCart.DeleteShoppingCartService;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.ws.rs.core.Response;
import java.util.Random;


/**
 * The Class DeleteShoppingCartControllerTest.
 */
@QuarkusTest
public class DeleteShoppingCartControllerTest {

	DeleteShoppingCartService deleteShoppingCartService;

	@Before
	public void setUp() {
		deleteShoppingCartService = Mockito.mock(DeleteShoppingCartService.class);
	}

	@Test
	public void shouldReturnOkFromDeleteRequest() throws ShoppingCartNotFoundException {
		DeleteShoppingCartController deleteShoppingCartController = new DeleteShoppingCartController(deleteShoppingCartService);
		Response response = deleteShoppingCartController.deleteShoppingCart(new Random().nextInt());
		Response expectedResponse =  Response.status(Response.Status.OK).build();

		Assert.assertEquals(expectedResponse.getStatus(), response.getStatus());
	}

	@Test (expected = ShoppingCartNotFoundException.class)
	public void shouldThrowShoppingCartNotFoundExceptionFromService() throws ShoppingCartNotFoundException {
		Mockito.doThrow(ShoppingCartNotFoundException.class).when(deleteShoppingCartService).execute(Mockito.any());
		DeleteShoppingCartController deleteShoppingCartController = new DeleteShoppingCartController(deleteShoppingCartService);
		deleteShoppingCartController.deleteShoppingCart(new Random().nextInt());
	}
}