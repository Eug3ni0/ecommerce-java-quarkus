package com.ecommerce.shop.adapter.api.shoppingcart;


import com.ecommerce.shop.application.service.shoppingCart.PostShoppingCartService;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartExistException;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartId;
import com.ecommerce.shop.domain.model.user.UserNotFoundException;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.ws.rs.core.Response;


/**
 * The Class PostShoppingCartController.
 */
@QuarkusTest
public class PostShoppingCartControllerTest {

	PostShoppingCartRequest postShoppingCartRequest;

	PostShoppingCartService postShoppingCartService;

	@Before
	public void setUp() {
		postShoppingCartRequest = Mockito.mock(PostShoppingCartRequest.class);
		postShoppingCartService = Mockito.mock(PostShoppingCartService.class);
	}

	@Test
	public void shouldReturnResponseCreated() throws UserNotFoundException, ShoppingCartExistException {
		ShoppingCartId shoppingCartId = Mockito.mock(ShoppingCartId.class);
		Mockito.when(postShoppingCartService.execute(Mockito.any())).thenReturn(shoppingCartId);
		PostShoppingCartController postShoppingCartController = new PostShoppingCartController(postShoppingCartService);
		Response response = postShoppingCartController.createShoppingCart(postShoppingCartRequest);
		Response expectedResponse =  Response.status(Response.Status.CREATED).build();

		Assert.assertEquals(expectedResponse.getStatus(), response.getStatus());
		Assert.assertEquals(expectedResponse.getEntity(), response.getEntity());
	}

	@Test (expected = UserNotFoundException.class)
	public void shouldThrowUserNotFoundExceptionFromService() throws UserNotFoundException, ShoppingCartExistException {
		Mockito.when(postShoppingCartService.execute(Mockito.any())).thenThrow(UserNotFoundException.class);
		PostShoppingCartController postShoppingCartController = new PostShoppingCartController(postShoppingCartService);
		postShoppingCartController.createShoppingCart(postShoppingCartRequest);
	}

	@Test (expected = ShoppingCartExistException.class)
	public void shouldThrowShoppingCartExistExceptionFromService() throws UserNotFoundException, ShoppingCartExistException {
		Mockito.when(postShoppingCartService.execute(Mockito.any())).thenThrow(ShoppingCartExistException.class);
		PostShoppingCartController postShoppingCartController = new PostShoppingCartController(postShoppingCartService);
		postShoppingCartController.createShoppingCart(postShoppingCartRequest);
	}
}