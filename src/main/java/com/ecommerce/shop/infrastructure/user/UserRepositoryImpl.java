package com.ecommerce.shop.infrastructure.user;


import com.ecommerce.shop.domain.model.user.UserId;
import com.ecommerce.shop.domain.model.user.UserNotFoundException;
import com.ecommerce.shop.domain.model.user.UserRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class UserRepositoryImpl implements UserRepository {

    private final List<UserId> userIds;

    @Inject
    public UserRepositoryImpl()
    {
        userIds = new ArrayList<>();
    }

    @Override
    public UserId create(UserId userId) {
        userIds.add(userId);
        return userId;
    }

    @Override
    public UserId load(UserId userId) throws UserNotFoundException {
        if (!userIds.contains(userId))
        {
            throw new UserNotFoundException(userId);
        }
        return userId;
    }
}