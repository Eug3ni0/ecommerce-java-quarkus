package com.ecommerce.shop.infrastructure.shoppingcart;


import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCart;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartId;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartRepository;
import com.ecommerce.shop.domain.model.user.UserId;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import javax.enterprise.context.ApplicationScoped;
import java.util.concurrent.TimeUnit;

@ApplicationScoped
public class ShoppingCartRepositoryImpl implements ShoppingCartRepository {

    Cache<UserId, ShoppingCart> memory;

    public ShoppingCartRepositoryImpl() {
        memory = CacheBuilder.newBuilder()
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .recordStats()
                .build();
    }

    @Override
    public ShoppingCartId create(ShoppingCart shoppingCart) {
        memory.put(shoppingCart.userId(), shoppingCart);
        return shoppingCart.shoppingCartId();
    }

    @Override
    public ShoppingCart load(UserId userId) throws ShoppingCartNotFoundException {
        ShoppingCart shoppingCart = memory.getIfPresent(userId);
        if (shoppingCart == null)
        {
            throw new ShoppingCartNotFoundException();
        }
        return shoppingCart;
    }

    @Override
    public void update(ShoppingCart shoppingCart) {
        memory.put(shoppingCart.userId(), shoppingCart);
    }

    @Override
    public void delete(ShoppingCart shoppingCart) {
        memory.invalidate(shoppingCart.userId());
    }
}