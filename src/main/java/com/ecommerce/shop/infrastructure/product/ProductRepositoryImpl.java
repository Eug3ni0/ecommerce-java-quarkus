package com.ecommerce.shop.infrastructure.product;


import com.ecommerce.shop.domain.model.product.Product;
import com.ecommerce.shop.domain.model.product.ProductId;
import com.ecommerce.shop.domain.model.product.ProductNotFoundException;
import com.ecommerce.shop.domain.model.product.ProductRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ApplicationScoped
public class ProductRepositoryImpl implements ProductRepository {

    static Map<ProductId, Product> map = new ConcurrentHashMap<>();

    @Override
    public ProductId insert(Product product)
    {
        map.put(product.productId(), product);
        return product.productId();
    }

    @Override
    public Product load(ProductId productId) throws ProductNotFoundException {
        Product product = map.get(productId);
        if (product == null){
            throw new ProductNotFoundException();
        }
        return product;
    }

    @Override
    public void update(Product product) {
        map.replace(product.productId(), product);
    }
}