package com.ecommerce.shop.adapter.api.product;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PostProductRequest {

    @NotNull(message = "The description is mandatory")
    @NotBlank(message = "The description must not be null")
    @JsonProperty("description")
    private String description;

    @Min(value = 1, message = "Amount should not be less than 1")
    @JsonProperty("amount")
    private int amount;

    public PostProductRequest() {
    }

    public String description()
    {
        return this.description;
    }

    public int amount()
    {
        return this.amount;
    }
}
