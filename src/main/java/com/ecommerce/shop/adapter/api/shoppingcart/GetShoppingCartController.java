package com.ecommerce.shop.adapter.api.shoppingcart;


import com.ecommerce.shop.application.service.shoppingCart.GetShoppingCartCommand;
import com.ecommerce.shop.application.service.shoppingCart.GetShoppingCartService;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCart;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("/shopping-cart")
public class GetShoppingCartController {

    private final GetShoppingCartService getShoppingCartService;

    @Inject
    public GetShoppingCartController(GetShoppingCartService getShoppingCartService) {
        this.getShoppingCartService = getShoppingCartService;

    }

    @GET
    @Path("/{userId}/get")
    public Response getShoppingCart(@PathParam("userId") int id) throws ShoppingCartNotFoundException {
        ShoppingCart shoppingCart;
        GetShoppingCartCommand getShoppingCartCommand = new GetShoppingCartCommand(id);
        shoppingCart =  getShoppingCartService.execute(getShoppingCartCommand);

        return Response.status(Response.Status.CREATED).entity(new Gson().toJson(shoppingCart)).build();
    }
}