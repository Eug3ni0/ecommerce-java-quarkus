package com.ecommerce.shop.adapter.api.shoppingcart.exceptionhandler;

import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ShoppingCartNotFoundExceptionHandler implements ExceptionMapper<ShoppingCartNotFoundException>
{
    @Override
    public Response toResponse(ShoppingCartNotFoundException exception)
    {
        return Response.status(Response.Status.BAD_REQUEST).entity(exception.getMessage()).build();
    }
}
