package com.ecommerce.shop.adapter.api.shoppingcart;


import com.ecommerce.shop.application.service.shoppingCart.DeleteShoppingCartCommand;
import com.ecommerce.shop.application.service.shoppingCart.DeleteShoppingCartService;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("/shopping-cart")
public class DeleteShoppingCartController {

    private final DeleteShoppingCartService deleteShoppingCartService;

    @Inject
    public DeleteShoppingCartController(DeleteShoppingCartService deleteShoppingCartService) {
        this.deleteShoppingCartService = deleteShoppingCartService;
    }

    @DELETE
    @Path("/{userId}/delete")
    public Response deleteShoppingCart(@PathParam("userId") int id) throws ShoppingCartNotFoundException {
        DeleteShoppingCartCommand deleteShoppingCartCommand = new DeleteShoppingCartCommand(id);
        deleteShoppingCartService.execute(deleteShoppingCartCommand);

        return Response.status(Response.Status.OK).build();
    }
}