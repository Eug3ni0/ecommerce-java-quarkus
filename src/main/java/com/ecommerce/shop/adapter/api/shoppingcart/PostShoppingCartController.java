package com.ecommerce.shop.adapter.api.shoppingcart;


import com.ecommerce.shop.application.service.shoppingCart.PostShoppingCartCommand;
import com.ecommerce.shop.application.service.shoppingCart.PostShoppingCartService;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartExistException;
import com.ecommerce.shop.domain.model.user.UserNotFoundException;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/shopping-cart")
public class PostShoppingCartController {

    private final PostShoppingCartService postShoppingCartService;

    @Inject
    public PostShoppingCartController(PostShoppingCartService postShoppingCartService) {
        this.postShoppingCartService = postShoppingCartService;
    }

    @POST
    @Path("/create")
    public Response createShoppingCart(@Valid PostShoppingCartRequest postShoppingCartRequest) throws UserNotFoundException, ShoppingCartExistException {
        PostShoppingCartCommand postShoppingCartCommand = new PostShoppingCartCommand(postShoppingCartRequest.userId());
        postShoppingCartService.execute(postShoppingCartCommand);

        return Response.status(Response.Status.CREATED).build();
    }
}