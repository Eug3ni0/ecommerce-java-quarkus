package com.ecommerce.shop.adapter.api.shoppingcart;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;

public class PostShoppingCartRequest {


    @Min(value = 1, message = "User id should not be less than 1")
    @JsonProperty("userId")
    private int userId;

    public PostShoppingCartRequest() {
    }

    public int userId()
    {
        return this.userId;
    }
}