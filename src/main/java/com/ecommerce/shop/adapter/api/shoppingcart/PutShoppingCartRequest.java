package com.ecommerce.shop.adapter.api.shoppingcart;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;

public class PutShoppingCartRequest {


    @Min(value = 1, message = "User id should not be less than 1")
    @JsonProperty("userId")
    private int userId;

    @Min(value = 1, message = "Product id should not be less than 1")
    @JsonProperty("productId")
    private int productId;

    @Min(value = 1, message = "Amount should not be less than 1")
    @JsonProperty("amount")
    private int amount;

    public PutShoppingCartRequest() {
    }

    public int userId()
    {
        return this.userId;
    }

    public int productId()
    {
        return this.productId;
    }

    public int amount()
    {
        return this.amount;
    }
}