package com.ecommerce.shop.adapter.api.shoppingcart;


import com.ecommerce.shop.application.service.shoppingCart.PutShoppingCartCommand;
import com.ecommerce.shop.application.service.shoppingCart.PutShoppingCartService;
import com.ecommerce.shop.domain.model.product.ProductNotFoundException;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;
import com.ecommerce.shop.domain.model.user.UserNotFoundException;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/shopping-cart")
public class PutShoppingCartController {

    private final PutShoppingCartService putShoppingCartService;

    @Inject
    public PutShoppingCartController(PutShoppingCartService putShoppingCartService) {
        this.putShoppingCartService = putShoppingCartService;
    }

    @PUT
    @Path("/insert-product")
    public Response insertProduct(@Valid PutShoppingCartRequest product) throws ProductNotFoundException, ShoppingCartNotFoundException, UserNotFoundException {

        PutShoppingCartCommand postShoppingCartCommand = new PutShoppingCartCommand(product.userId(), product.productId(), product.amount());
        putShoppingCartService.execute(postShoppingCartCommand);
        return Response.status(Response.Status.OK).build();
    }
}