package com.ecommerce.shop.adapter.api.shoppingcart.exceptionhandler;

import com.ecommerce.shop.domain.model.user.UserNotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UserNotFoundExceptionHandler implements ExceptionMapper<UserNotFoundException>
{
    @Override
    public Response toResponse(UserNotFoundException exception)
    {
        return Response.status(Response.Status.BAD_REQUEST).entity(exception.getMessage()).build();
    }
}
