package com.ecommerce.shop.adapter.api.product;


import com.ecommerce.shop.application.service.product.ProductCommand;
import com.ecommerce.shop.application.service.product.ProductService;
import com.ecommerce.shop.domain.model.product.ProductId;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;


@Path("/product")
public class PostProductController {

    private final ProductService productService;

    @Inject
    public PostProductController(ProductService productService) {
        this.productService = productService;
    }

    @POST
    @Path("/insert-product")
    public Response insertProduct(@Valid PostProductRequest postProductRequest) throws ConstraintViolationException {

        ProductId productId;
        ProductCommand productCommand = new ProductCommand(postProductRequest.description(), postProductRequest.amount());
        productId = productService.execute(productCommand);

        return Response.status(Response.Status.CREATED).entity(new Gson().toJson(productId)).build();
    }
}