package com.ecommerce.shop.adapter.api.shoppingcart.exceptionhandler;

import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartExistException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ShoppingCartExistExceptionHandler implements ExceptionMapper<ShoppingCartExistException>
{
    @Override
    public Response toResponse(ShoppingCartExistException exception)
    {
        return Response.status(Response.Status.BAD_REQUEST).entity(exception.getMessage()).build();
    }
}
