package com.ecommerce.shop.adapter.api.user;


import com.ecommerce.shop.application.service.user.UserCommand;
import com.ecommerce.shop.application.service.user.UserService;
import com.ecommerce.shop.domain.model.user.UserId;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/user")
public class PostUserController {

    private final UserService userService;

    @Inject
    public PostUserController(UserService userService) {
        this.userService = userService;
    }

    @POST
    @Path("/create")
    public Response createUser() {

        UserId userId = userService.execute(new UserCommand());
        return Response.status(Response.Status.CREATED).entity(new Gson().toJson(userId)).build();
    }
}