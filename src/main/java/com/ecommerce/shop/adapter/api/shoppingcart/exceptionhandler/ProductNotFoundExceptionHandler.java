package com.ecommerce.shop.adapter.api.shoppingcart.exceptionhandler;

import com.ecommerce.shop.domain.model.product.ProductNotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ProductNotFoundExceptionHandler implements ExceptionMapper<ProductNotFoundException>
{
    @Override
    public Response toResponse(ProductNotFoundException exception)
    {
        return Response.status(Response.Status.BAD_REQUEST).entity(exception.getMessage()).build();
    }
}
