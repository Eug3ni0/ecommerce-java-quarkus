package com.ecommerce.shop.domain.model.shoppingcart;


import com.ecommerce.shop.domain.model.user.UserId;

/**
 * The Interface Shopping cart repository.
 */
public interface ShoppingCartRepository {

	public ShoppingCartId create(ShoppingCart shoppingCart);

	public ShoppingCart load(UserId userId) throws ShoppingCartNotFoundException;

	public void update(ShoppingCart shoppingCart);

	public void delete(ShoppingCart shoppingCart);
}
