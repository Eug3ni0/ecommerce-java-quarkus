package com.ecommerce.shop.domain.model.product;

/**
 * The Interface ProductRepository.
 */
public interface ProductRepository {

	public ProductId insert(Product product);

	public Product load(ProductId productId) throws ProductNotFoundException;

	public void update(Product product);
}
