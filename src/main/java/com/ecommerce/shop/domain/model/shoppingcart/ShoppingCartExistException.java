package com.ecommerce.shop.domain.model.shoppingcart;

import com.google.gson.Gson;

public class ShoppingCartExistException extends Exception {

    public ShoppingCartExistException(ShoppingCart shoppingCart) {
            super("You currently have a cart: " + new Gson().toJson(shoppingCart));
        }
}

