package com.ecommerce.shop.domain.model.user;

public class UserNotFoundException extends Exception {

    public UserNotFoundException(UserId userId) {
            super("User" + userId.id() + " not found");
        }
}

