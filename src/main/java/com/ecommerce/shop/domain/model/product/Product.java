package com.ecommerce.shop.domain.model.product;

public class Product {

    /**
     * The product id
     */
    private ProductId productId;

    /**
     * The description
     */
    private String description;

    /**
     * The amount
     */
    private int amount;

    /**
     * Instantiates a new product
     *
     * @param productId     the product id
     * @param description   the description
     * @param amount        the amount
     */
    public Product(ProductId productId, String description, int amount) {
        this.productId = productId;
        this.description = description;
        this.amount = amount;
    }

    public ProductId productId()
    {
        return productId;
    }

    public String description()
    {
        return description;
    }

    public int amount()
    {
        return amount;
    }

    public void updateProduct(Product product)
    {
        this.description = product.description;
        this.amount = product.amount;
    }
}