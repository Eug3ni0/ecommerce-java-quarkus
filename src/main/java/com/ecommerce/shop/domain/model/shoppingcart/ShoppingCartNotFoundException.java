package com.ecommerce.shop.domain.model.shoppingcart;

public class ShoppingCartNotFoundException extends Exception {

    public ShoppingCartNotFoundException() {
            super("Shopping cart not available");
        }
}

