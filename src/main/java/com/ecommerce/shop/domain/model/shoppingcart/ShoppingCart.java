package com.ecommerce.shop.domain.model.shoppingcart;

import com.ecommerce.shop.domain.model.product.Product;
import com.ecommerce.shop.domain.model.user.UserId;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ShoppingCart {

    /**
     * The shopping cart id
     */
    private final ShoppingCartId shoppingCartId;

    /**
     * The userId
     */
    private final UserId userId;

    /**
     * The product id
     */
    private final List<Product> product;

    /**
     * Instantiates a new product
     *
     * @param shoppingCartId   the shopping cart id
     * @param userId           the user id
     */
    public ShoppingCart(ShoppingCartId shoppingCartId, UserId userId) {
        this.shoppingCartId = shoppingCartId;
        this.userId = userId;
        this.product = new ArrayList<>();
    }

    public UserId userId()
    {
        return userId;
    }

    public ShoppingCartId shoppingCartId()
    {
        return shoppingCartId;
    }

    public List<Product> product()
    {
        return product;
    }


    public void addToShoppingCart(Product product)
    {
        this.product.add(product);
    }

    public void updateShoppingCart(Product product)
    {
        ListIterator<Product> iterator1 = this.product.listIterator();
        while (iterator1.hasNext()){
            Product productInCart = iterator1.next();
            if (productInCart.productId().id() == product.productId().id())
            {
                iterator1.set(product);
            }else {
                iterator1.add(product);
            }
        }
    }

}