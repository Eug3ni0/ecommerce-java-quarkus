package com.ecommerce.shop.domain.model.user;


/**
 * The User repository.
 */
public interface UserRepository {

	public UserId create(UserId userId);

	public UserId load(UserId userId) throws UserNotFoundException;
}
