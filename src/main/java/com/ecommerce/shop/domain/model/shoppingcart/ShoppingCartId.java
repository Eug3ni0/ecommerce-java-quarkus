package com.ecommerce.shop.domain.model.shoppingcart;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *  The class ShoppingCartId
 */
public class ShoppingCartId {

    private static final AtomicInteger count = new AtomicInteger(0);

    private final int id;

    /**
     * Instantiates a new shopping cart id
     */
    public ShoppingCartId() {
        this.id = count.incrementAndGet();
    }

    /**
     * Gets the id
     *
     * @return value of id
     */
    public int id() {
        return id;
    }
}
