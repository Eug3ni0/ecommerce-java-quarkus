package com.ecommerce.shop.domain.model.product;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *  The class ProductId
 */
public class ProductId {

    private static final AtomicInteger count = new AtomicInteger(0);

    private final int id;

    /**
     * Instantiates a new product id
     */
    public ProductId() {
        this.id = count.incrementAndGet();
    }

    /**
     * Instantiates a new product id
     *
     * @param id the id
     */
    public ProductId(int id){
        this.id = id;
    }

    /**
     * Gets the id
     *
     * @return value of id
     */
    public int id() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductId productId = (ProductId) o;
        return id == productId.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
