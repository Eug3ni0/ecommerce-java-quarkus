package com.ecommerce.shop.domain.model.user;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *  The class ProductId
 */
public class UserId {

    private static final AtomicInteger count = new AtomicInteger(0);

    private final int id;

    /**
     * Instantiates a new product id
     */
    public UserId() {
        this.id = count.incrementAndGet();
    }

    /**
     * Instantiates a new product id
     */
    public UserId(int id) {
        this.id = id;
    }

    /**
     * Gets the id
     *
     * @return value of id
     */
    public int id() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserId userId = (UserId) o;
        return id == userId.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
