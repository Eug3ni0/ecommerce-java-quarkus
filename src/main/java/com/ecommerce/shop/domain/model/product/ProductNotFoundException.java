package com.ecommerce.shop.domain.model.product;

public class ProductNotFoundException extends Exception {

    public ProductNotFoundException() {
            super("Product not found");
        }
}

