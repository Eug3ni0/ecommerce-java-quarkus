package com.ecommerce.shop.application.service.shoppingCart;


public class DeleteShoppingCartCommand {

    private int id;

    public DeleteShoppingCartCommand(int id) {
        this.id = id;
    }

    /**
     * Gets the id
     *
     * @return value of id
     */
    public int id() {
        return id;
    }

}
