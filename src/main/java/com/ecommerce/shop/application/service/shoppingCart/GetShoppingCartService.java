package com.ecommerce.shop.application.service.shoppingCart;


import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCart;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartRepository;
import com.ecommerce.shop.domain.model.user.UserId;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class GetShoppingCartService {

    private ShoppingCartRepository shoppingCartRepository;

    @Inject
    public GetShoppingCartService(ShoppingCartRepository shoppingCartRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
    }

    public ShoppingCart execute(GetShoppingCartCommand command) throws ShoppingCartNotFoundException {
        return shoppingCartRepository.load(new UserId(command.id()));
    }
}
