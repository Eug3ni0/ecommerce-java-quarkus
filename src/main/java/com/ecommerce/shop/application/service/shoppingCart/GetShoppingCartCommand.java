package com.ecommerce.shop.application.service.shoppingCart;


public class GetShoppingCartCommand {

    private int id;

    public GetShoppingCartCommand(int id) {
        this.id = id;
    }

    /**
     * Gets the id
     *
     * @return value of id
     */
    public int id() {
        return id;
    }

}
