package com.ecommerce.shop.application.service.product;

public class ProductCommand {

    private final String description;

    private final int amount;

    public ProductCommand(String description, int amount) {
        this.description = description;
        this.amount = amount;
    }

    public String description()
    {
        return this.description;
    }

    public int amount()
    {
        return this.amount;
    }
}
