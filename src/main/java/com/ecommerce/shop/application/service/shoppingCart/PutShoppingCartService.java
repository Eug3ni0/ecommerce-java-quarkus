package com.ecommerce.shop.application.service.shoppingCart;


import com.ecommerce.shop.domain.model.product.Product;
import com.ecommerce.shop.domain.model.product.ProductId;
import com.ecommerce.shop.domain.model.product.ProductNotFoundException;
import com.ecommerce.shop.domain.model.product.ProductRepository;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCart;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartRepository;
import com.ecommerce.shop.domain.model.user.UserId;
import com.ecommerce.shop.domain.model.user.UserNotFoundException;
import com.ecommerce.shop.domain.model.user.UserRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PutShoppingCartService {

    private final ShoppingCartRepository shoppingCartRepository;

    private final ProductRepository productRepository;

    private final UserRepository userRepository;

    @Inject
    public PutShoppingCartService(ShoppingCartRepository shoppingCartRepository, ProductRepository productRepository, UserRepository userRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.productRepository = productRepository;
        this.userRepository = userRepository;
    }

    public void execute(PutShoppingCartCommand command) throws ProductNotFoundException, ShoppingCartNotFoundException, UserNotFoundException {
        Product product = productRepository.load(new ProductId(command.productId()));
        Product productToInsertInCart = new Product(product.productId(), product.description(), command.amount());
        UserId userId = userRepository.load(new UserId(command.userId()));
        ShoppingCart shoppingCart = shoppingCartRepository.load(userId);
        shoppingCart.addToShoppingCart(productToInsertInCart);
        shoppingCartRepository.update(shoppingCart);
    }
}
