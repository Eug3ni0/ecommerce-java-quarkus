package com.ecommerce.shop.application.service.product;

import com.ecommerce.shop.domain.model.product.ProductId;
import com.ecommerce.shop.domain.model.product.Product;
import com.ecommerce.shop.domain.model.product.ProductRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class ProductService {

    private ProductRepository productRepository;

    @Inject
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public ProductId execute(ProductCommand productCommand) {
        Product product = new Product(new ProductId(), productCommand.description(), productCommand.amount());
        return productRepository.insert(product);
    }
}
