package com.ecommerce.shop.application.service.shoppingCart;


import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCart;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartNotFoundException;
import com.ecommerce.shop.domain.model.shoppingcart.ShoppingCartRepository;
import com.ecommerce.shop.domain.model.user.UserId;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class DeleteShoppingCartService {

    private final ShoppingCartRepository shoppingCartRepository;

    @Inject
    public DeleteShoppingCartService(ShoppingCartRepository shoppingCartRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
    }

    public void execute(DeleteShoppingCartCommand command) throws ShoppingCartNotFoundException {
        ShoppingCart shoppingCart = shoppingCartRepository.load(new UserId(command.id()));
        shoppingCartRepository.delete(shoppingCart);
    }
}
