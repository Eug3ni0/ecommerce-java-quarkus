package com.ecommerce.shop.application.service.user;

import com.ecommerce.shop.domain.model.user.UserId;
import com.ecommerce.shop.domain.model.user.UserRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class UserService {

    private final UserRepository userRepository;

    @Inject
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserId execute(UserCommand command) {
        return userRepository.create(new UserId());
    }
}
