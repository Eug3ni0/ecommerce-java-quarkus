package com.ecommerce.shop.application.service.shoppingCart;


import com.ecommerce.shop.domain.model.shoppingcart.*;
import com.ecommerce.shop.domain.model.user.UserId;
import com.ecommerce.shop.domain.model.user.UserNotFoundException;
import com.ecommerce.shop.domain.model.user.UserRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PostShoppingCartService {

    private final ShoppingCartRepository shoppingCartRepository;
    
    private final UserRepository userRepository;

    @Inject
    public PostShoppingCartService(ShoppingCartRepository shoppingCartRepository, UserRepository userRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.userRepository = userRepository;
    }

    public ShoppingCartId execute(PostShoppingCartCommand command) throws UserNotFoundException, ShoppingCartExistException {
        UserId userId = userRepository.load(new UserId(command.userId()));
        ShoppingCart shoppingCart = null;
        try {
            shoppingCart = shoppingCartRepository.load(userId);
        } catch (ShoppingCartNotFoundException shoppingCartNotFoundException) {
           return shoppingCartRepository.create(new ShoppingCart(new ShoppingCartId(), userId));
        }
        throw new ShoppingCartExistException(shoppingCart);
    }
}
