package com.ecommerce.shop.application.service.shoppingCart;


public class PutShoppingCartCommand {

    private int userId;

    private int productId;

    private int amount;

    public PutShoppingCartCommand(int userId, int productId, int amount) {
        this.userId = userId;
        this.productId = productId;
        this.amount = amount;
    }

    /**
     * Gets the id
     *
     * @return value of id
     */
    public int userId() {
        return userId;
    }

    /**
     * Gets the productId
     *
     * @return value of productId
     */
    public int productId() {
        return productId;
    }

    /**
     * Gets the amount
     *
     * @return value of amount
     */
    public int amount() {
        return amount;
    }
}
