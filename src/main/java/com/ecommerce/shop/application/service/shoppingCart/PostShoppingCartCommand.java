package com.ecommerce.shop.application.service.shoppingCart;


public class PostShoppingCartCommand {

    private int userId;

    public PostShoppingCartCommand(int userId) {
        this.userId = userId;
    }

    /**
     * Gets the id
     *
     * @return value of id
     */
    public int userId() {
        return userId;
    }
}
