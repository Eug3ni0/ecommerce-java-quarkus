# ecommerce project

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

## Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

## Examples curl requests in order to test application

Create user:
```shell script
curl --location --request POST 'http://localhost:8080/user/create'
```

Create shopping cart:
```shell script
curl --location --request POST 'http://localhost:8080/shopping-cart/create' \
--header 'Content-Type: application/json' \
--data-raw '{
"userId": 1
}'
```

Create product:
```shell script
curl --location --request POST 'http://localhost:8080/product/insert-product' \
--header 'Content-Type: application/json' \
--data-raw '{
    "description": "New Product",
    "amount": 10
}'
```

Insert product in shopping cart: 
```shell script
curl --location --request PUT 'http://localhost:8080/shopping-cart/insert-product' \
--header 'Content-Type: application/json' \
--data-raw '{
    "userId": 1,
    "productId": 1,
    "amount": 3
}'
```

Get shopping cart: 
```shell script
curl --location --request GET 'http://localhost:8080/shopping-cart/1/get'
```

Delete shopping cart: 
```shell script
curl --location --request DELETE 'http://localhost:8080/shopping-cart/1/delete'
```

